const express = require('express');
const path = require('path');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;


const isDev = process.env.NODE_ENV !== 'production';
const PORT = process.env.PORT || 5000;

const app = express();

const http = require('http').Server(app);
// login exports
const bodyParser = require('body-parser');
const exjwt = require('express-jwt');
const jwt = require('jsonwebtoken');
const config = require('./config.json');
const errorHandler = require('./_helpers/error-handler');
const userLoginHandler = require('./login/userLogin.service');

// mongoDB and mongoose export and connection
const mongoose = require('mongoose');
mongoose.connect('mongodb://student:student1@ds259111.mlab.com:59111/navy', {useNewUrlParser: true, useUnifiedTopology: true });

const Users = require('./schemas/Users');
const Product = require('./schemas/Product');
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('DB CONNECTED')
  // we're connected!
});

// login insertion starts here
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
  next();
});

// Setting up bodyParser to use json and set it to req.body
app.use(bodyParser.json({ limit: '16mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '16mb' }));

// LOGIN ROUTE
app.post('/api/login', userLoginHandler);

app.use(errorHandler);

// login insertion ends here

// Multi-process to utilize all CPU cores.
if (!isDev && cluster.isMaster) {
  console.error(`Node cluster master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.error(`Node cluster worker ${worker.process.pid} exited: code ${code}, signal ${signal}`);
  });

} else {
  // used when we try to get logged in user's data with a token
  const checkToken = (req, res, next) => {
    const header = req.headers['authorization'];

    if(typeof header !== 'undefined') {
        const bearer = header.split(' ');
        const token = bearer[1];

        req.token = token;
        next();
    } else {
        //If header is undefined return Forbidden (403)
        res.sendStatus(403)
    }
}

  // Priority serve any static files.
  app.use(express.static(path.resolve(__dirname, '../react-ui/build')));

  // Answer API requests.
  app.get('/api', function (req, res) {
    res.set('Content-Type', 'application/json');
    res.send('{"message":"Hello from the custom server!"}');
  });

  app.post('/api/add_user', function (req, res) {
    Users.findOne({email : req.body.email}).then(result=> {
      if (!result) {
        Users.create(req.body).then(result=>res.send(result.data = 'New user has been created!'));
      } else {
        res.send(result.data = 'This email is already used');
      }
    });
  });

  app.post('/api/add_product', function (req, res) {
    Product.create(req.body).then(result=>res.send(result.data = 'Product was added!'));
  });


  // get user when openning website and there's a token in local storage
  app.get('/api/get_logged_user', checkToken, (req, res) => {
    //verify the JWT token sent from the front-end for the user
    jwt.verify(req.token, config.secret, (err, authorizedData) => {
        if(err){
            //If error send Forbidden (403)
            console.log('ERROR: Could not connect to the protected route');
            res.sendStatus(403);
        } else {
          Users.findOne({_id : authorizedData.id}).then(result=> {
            if (result) {
              res.json({
                message: 'Successful log in',
                user: result
              });
            } else {
              res.send(result.data = 'error occured');
            }
          })
        }
    })
  });

  app.post('/api/products', function (req, res) {

    if (req.body.userId) {
      Product.find({author: req.body.userId}, function(err, docs) {
      if (!err){
        res.send(docs);
      } else {throw err;}
    });
    } else {
      Product.find({}, function(err, docs) {
        if (!err){
          res.send(docs);
        } else {throw err;}
      });
    }
  });

  app.get('/api/product/:id', async function (req, res) {
    try {
      const id = req.params.id;
      const singleProduct = await Product.findById(id)
      .populate('author', 'firstName lastName phone email profilePhoto')
      .exec();
      res.send(singleProduct);
    }
    catch (err) {
      throw err;
    };
  });


  app.put('/api/update_product', function (req, res) {
    const { productID, ...updateData } = req.body;

      Product.findOneAndUpdate({ _id : productID}, updateData, {useFindAndModify: false})
        .then(result=>{
          res.send('Product updated');
        });

  });
  app.put('/api/book_days', function (req, res) {
    const { productID, disabledDays } = req.body;
      Product.findOneAndUpdate({ _id : productID }, { disabledDays: disabledDays }, {useFindAndModify: false})
        .then(result=>{
          res.send(result.disabledDays);
        });
  })

  app.put('/api/update_user', function (req, res) {
    const { userId, ...updateData } = req.body;
    let presentEmail;

    if (!updateData.email) {
      presentEmail = 'no email';
    } else {
      presentEmail = updateData.email;
    }

    Users.findOne({email : presentEmail}).then(user=>{
      if (user) {
        res.send(res.data = 'this email is already used')
      } else {
        Users.findOneAndUpdate({ _id : userId}, updateData, {useFindAndModify: false})
          .then(result=>{
            Users.findById(userId, (err, resp) => {
            res.send(resp);
            })
          });
      }
    })
  });

  app.get('/api/users', function (req, res) {
    Users.find({}, function(err, docs) {
      if (!err){
          res.send(docs);
      } else {throw err;}
    });
  })
  /* app.post('/api', function (req, res) {
    res.set('Content-Type', 'application/json');
    res.send('{"message":"Hello from the custom server!"}');
  }); */

  require('./chat/routes/chat')(app);
  require('./chat/socket')(http);

  // All remaining requests return the React app, so it can handle routing.
  app.get('*', function(request, response) {
    response.sendFile(path.resolve(__dirname, '../react-ui/build', 'index.html'));
  });

  http.listen(PORT, function () {
    console.error(`Node ${isDev ? 'dev server' : 'cluster worker '+process.pid}: listening on port ${PORT}`);
  });
}

app.post('/api/filter', function(req, res) {
  const { category, searchValue } = req.body;
  const findBy = {
    ...(category && { category }),
    ...(searchValue && { description: new RegExp(`${searchValue}`) })
  };
  try {
    Product.find(findBy, function(err, docs) {
      if (!err) {
        res.send(docs);
      } else {
        throw err;
      }
    });
  } catch (err) {
    throw err;
  }
});
