var mongoose = require('mongoose');

function formattedDate() {
  var formattedDate = new Date();
  var fullDate;
  var month = formattedDate.toLocaleString('en-US', {
    month: 'short'
  });
  var day = formattedDate.toLocaleString('en-US', {
    day: 'numeric'
  });
  var year = formattedDate.toLocaleString('en-US', {
    year: 'numeric'
  });

  fullDate = `${month} ${day} ${year}`;
  return fullDate;
}

var productSchema = new mongoose.Schema({
  productName: String,
  productPhoto: { type: String, default: 'none'},
  description: String,
  longDescription: String,
  category: String,
  price: String,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users'
  },
  dateForShow: {
    type: String,
    default: formattedDate(),
  },
  date: {
    type: Date,
    default: Date.now
  },
  disabledDays: {
    type: Array,
    default: [],
  },
});

var Product = mongoose.model('Product', productSchema);

module.exports = Product;
