var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    phone: String,
    email: String,
    password: String,
    profilePhoto: { type: String, default: 'none'},
    date: { type: Date, default: Date.now },
});

var Users = mongoose.model('Users', userSchema);

module.exports = Users;
