const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChatSchema = new Schema({
  productId: String, // Schema.Types.ObjectId,
  owner: { type: Schema.Types.ObjectId, ref: 'Users' },
  shopper: { type: Schema.Types.ObjectId, ref: 'Users' },
}, { toJSON: { virtuals: true } });

ChatSchema.virtual('chatHistory', {
  ref: 'Messages',
  localField: '_id',
  foreignField: 'chatId',
  options: { sort: { date: 1 } },
});

const MessagesSchema = new Schema({
  chatId: Schema.Types.ObjectId,
  text: String,
  authorId: Schema.Types.ObjectId,
  date: {type: Date, default: Date.now},
  isReaded: {type: Boolean, default: false}
});

const Chat = mongoose.model('Chat', ChatSchema);
const Messages = mongoose.model('Messages', MessagesSchema);

module.exports = {Chat, Messages};
