const mongoose = require('mongoose');
const {Chat, Messages} = require('../models/Chat');

module.exports = server => {

  server.get('/api/myChats/:userID', async (req, res) => {
    try {
      const chats = await Chat
        .find({$or:[{shopper: req.params.userID},{owner: req.params.userID}]})
        .populate('owner', 'firstName lastName profilePhoto')
        .populate('shopper', 'firstName lastName profilePhoto')
        .exec();
      res.send(chats);
    } catch (err) {
      throw err;
    }
  });

  server.get('/api/singleChat/:productId/:owner/:shopper', async (req, res) => {
    try {
      const {productId, owner, shopper} = req.params;
      const chatHistory = await Chat.find({productId, owner, shopper}).populate('chatHistory').exec();
      res.send(chatHistory[0]);
    } catch (err) {
      throw err;
    }
  });

  server.post('/createChat', async (req, res) => {
    const { productId, owner, shopper } = req.body;
    try {
      const findedChat = await Chat.find({productId, owner, shopper});
      const chatExist = findedChat.length;
      if(!chatExist){
        const chat = new Chat({
          productId,
          owner,
          shopper
        });
        const newChat = await chat.save((err, savedChat) => {
          if (err) throw err;
          res.send(savedChat);
        });
      } else {
        res.send(findedChat[0]);
      }
    } catch (error) {
      throw error;
    }
  });


  server.post('/newMessage', async (req, res) => {
    const { chatId, text, authorId } = req.body;
    const message = new Messages({ chatId, text, authorId });
    try {
      const newMessage = await message.save((err, msg) => {
        if (err) throw err;
        res.send(msg);
      });
    } catch (error) {
      throw error;
    }
  });

};
