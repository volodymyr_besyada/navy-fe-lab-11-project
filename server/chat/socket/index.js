module.exports = http => {
  const io = require('socket.io')(http);

  const onlineUsers = {}; // key = userId, val = socket.id

  io.on('connection', function(socket) {
    let {userId} = socket.handshake.query;
    console.log('----- 1) connection event store before push new user');
    console.log(onlineUsers);
    console.log('----- 1) ');

    if(!onlineUsers[userId]) {
      onlineUsers[userId] = socket.id;
    }
    console.log('----- 2) connection event store after push new user');
    console.log(onlineUsers);
    console.log('----- 2) ');

    socket.on("new message", function(data) {
      io.to(onlineUsers[data.to]).emit("receive new message", data.newMessage);
    });

    socket.on("disconnect", () => {
      console.log('-------');
      console.log("User is disconnected");
      console.log('-------');

      for(let key in onlineUsers) {
        if(onlineUsers[key] === socket.id) {
          delete onlineUsers[key];
        }
      }

      console.log('----- 3) disconnection event store');
      console.log(onlineUsers);
    });
  });
}
