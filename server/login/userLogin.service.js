const jwt = require('jsonwebtoken');
const config = require('../config.json');
const Users = require('../schemas/Users');

const userLoginHandler = (req, res) => {
    const { email, password } = req.body;
    Users.findOne({email, password}, function(err, user) {
        if (user) {
            let token = jwt.sign({ id: user._id, email: email }, config.secret, { expiresIn: 129600 }); // Signing the token
    
            res.json({
                success: true,
                err: null,
                token,
                user
            });
        } else {
            res.status(401).json({
                success: false,
                token: null,
                err: 'Username or password is incorrect'
            });
        }
    })
};

module.exports = userLoginHandler;