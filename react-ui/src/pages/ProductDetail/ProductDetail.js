import React, { Component } from 'react';
import { connect } from 'react-redux';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import DayPicker, { DateUtils } from 'react-day-picker';
import axios from 'axios';
import classes from './ProductDetail.module.scss';
import AuthService from '../../services/AuthService';
import Spinner from '../../components/Spinner/Spinner';
import { fetchProductDetail } from '../../store/actions/productDetail';
import 'react-day-picker/lib/style.css';

class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.handleOwnerDayClick = this.handleOwnerDayClick.bind(this);
    this.handleViewerDayClick = this.handleViewerDayClick.bind(this);
    this.hundleCreateChat = this.hundleCreateChat.bind(this);
    this.state = {
      selectedDays: [],
      allBookedDays: [],
      disabledDays: [],
      calendarLoading: false,
    };
  }

  componentDidMount() {
    const { match } = this.props;
    const { id } = match.params;
    const { fetchProductDetail: getProduct } = this.props;
    getProduct(id).then((product) => {
      const disabledDays = product.disabledDays.map((day) => new Date(day));
      this.setState({ allBookedDays: disabledDays });
      this.setState({ disabledDays });
    });
  }

  saveBookedDays = () => {
    const { allBookedDays } = this.state;
    const { match: { params: { id } } } = this.props;

    this.setState({ calendarLoading: true });
    axios.put('/api/book_days', {
      productID: id,
      disabledDays: allBookedDays,
    })
      .then(() => {
        this.setState({ calendarLoading: false });
      })
      .catch((err) => {
        this.setState({ calendarLoading: false });
        // eslint-disable-next-line
          console.log(err);
        // eslint-disable-next-line
          alert('it didn\'t go well, try again later please');
      });
  }

  hundleCreateChat() {
    const { history: { push }, currentUserId } = this.props;
    // const { currentUserId } = this.props;
    if (currentUserId) {
      axios.post('/createChat', {
        /* eslint-disable */
        productId: this.props.product._id,
        owner: this.props.product.author._id,
        /* eslint-enable */
        shopper: currentUserId,
      }).then((res) => {
        const {
          productId,
          owner,
          shopper,
          _id,
        } = res.data;
        // eslint-disable-next-line
        const bookDays = this.state.selectedDays;
        if (bookDays.length) {
          let text = 'Hi, I want book some item on day: \n';
          bookDays.forEach((day) => {
            text += `${day.toLocaleDateString()}\n`;
          });
          const authorId = currentUserId;
          const chatId = _id;
          axios.post('/newMessage', {
            text, authorId, chatId,
          }).then(() => {
            push(`/chat/${productId}/${owner}/${shopper}`);
          });
        } else {
          push(`/chat/${productId}/${owner}/${shopper}`);
        }
      });
    }
  }

  handleOwnerDayClick(day, { selected }) {
    const { allBookedDays } = this.state;

    if (selected) {
      const selectedIndex = allBookedDays.findIndex((selectedDay) => (
        DateUtils.isSameDay(selectedDay, day)
      ));
      allBookedDays.splice(selectedIndex, 1);
    } else {
      allBookedDays.push(day);
    }
    this.setState({ allBookedDays });
  }

  handleViewerDayClick(day, modifiers = {}) {
    const { disabled, selected } = modifiers;
    const { selectedDays } = this.state;

    if (disabled) {
      return;
    }

    if (selected) {
      const selectedIndex = selectedDays.findIndex((selectedDay) => (
        DateUtils.isSameDay(selectedDay, day)
      ));
      selectedDays.splice(selectedIndex, 1);
    } else {
      selectedDays.push(day);
    }
    this.setState({ selectedDays });
  }

  render() {
    const {
      calendarLoading, allBookedDays, selectedDays, disabledDays,
    } = this.state;
    const { loading, product } = this.props;
    const Auth = new AuthService();

    // Get product author
    let authorID;
    let profilePhoto;
    if (product.author) {
      authorID = product.author._id;
      profilePhoto = product.author.profilePhoto;
    }
    // Check for user logged in.
    const isLoggedIn = Auth.loggedIn();
    // Current logged user
    let loggedUserID = null;
    // Author product.
    let ownProduct = false;

    if (isLoggedIn) {
      const profile = Auth.getProfile();
      loggedUserID = profile.id;
      ownProduct = loggedUserID === authorID;
    }

    return (
      <Container className={classes.DetailWrapper}>
        <Spinner loading={loading || calendarLoading} />
        <Grid container spacing={3}>
          <Grid item sm={12} md={5}>
            <div className={classes.MainProductImage}>
              {
                (product.productPhoto !== 'none')
                  ? <img src={product.productPhoto} alt="Product detail" style={{ boxShadow: '0 0 25px #ccc' }} />
                  : <img src="http://placeimg.com/640/480/any" alt="Product detail" />
              }
            </div>
          </Grid>
          <Grid item sm={12} md={7} className={classes.MainInfo}>
            <Grid container spacing={3}>
              <Grid item sm={6} md={6}>
                <div>
                  <h3>
                    {ownProduct ? 'Book The Product' : 'See When The Product Is Available'}
                  </h3>
                  { ownProduct
                    ? (
                      <DayPicker
                        selectedDays={allBookedDays}
                        onDayClick={this.handleOwnerDayClick}
                      />
                    )
                    : (
                      <DayPicker
                        selectedDays={selectedDays}
                        disabledDays={disabledDays}
                        onDayClick={this.handleViewerDayClick}
                      />
                    )}
                  {
                    ownProduct
                      ? (
                        <div>
                          <Button
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={this.saveBookedDays}
                          >
                        Save
                          </Button>
                        </div>
                      )
                      : (
                        <div>
                          <Button
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={this.hundleCreateChat}
                          >
                          Start Chat
                          </Button>
                        </div>
                      )
                  }
                </div>
              </Grid>
              <Grid item sm={6} md={6}>
                <p className={classes.Price}>
                  Rent/day:
                  {' '}
                  <span>
                    {product.price}
                    $
                  </span>
                </p>
                <div className={classes.Owner}>
                  Owner:
                  {
                (profilePhoto && profilePhoto !== 'none')
                  ? (
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <img
                        src={profilePhoto}
                        alt="Product detail"
                        style={{
                          width: '100px', height: '100px', borderRadius: '50%', margin: '10px',
                        }}
                      />
                      <div>
                        <p>{`${product.author.firstName}`}</p>
                        <p>{`${product.author.lastName}`}</p>
                      </div>
                    </div>
                  )
                  : (
                    <span>
                      { product.author && ` ${product.author.firstName} ${product.author.lastName}`}
                    </span>
                  )
}
                </div>
                <p className={classes.PhoneNumber}>
                  Phone:
                  {' '}
                  <span>
                    { product.author ? product.author.phone : ''}
                  </span>
                </p>
                <p className={classes.ProductCategory}>
                  Category:
                  {' '}
                  <span>{product.category}</span>
                </p>
                <p className={classes.ProductInfo}>
                  Added:
                  {' '}
                  <span>
                    {product.dateForShow}
                  </span>
                </p>
                {ownProduct
                  && (
                  <Link to={`/edit-product/${product._id}`} className={classes.newProductLink}>
                    <Button
                      variant="contained"
                      className={classes.button}
                    >
                        Edit product
                    </Button>
                  </Link>
                  )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item md={12}>
            <div>
              <hr />
              <h1>
                {product.productName}
              </h1>
              <p className={classes.ProductDescription}>
                <span>Description: </span>
                {product.description}
              </p>
              <p className={classes.ProductDetails}>
                <span>Details: </span>
                {product.longDescription}
              </p>
            </div>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  product: state.product.item || [],
  loading: state.product.loading,
  error: state.product.error,
  currentUserId: state.user.user._id,
});

const mapDispatchToProps = (dispatch) => ({
  fetchProductDetail: (id) => dispatch(fetchProductDetail(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);

ProductDetail.propTypes = {
  // eslint-disable-next-line
  product: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  fetchProductDetail: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
  // eslint-disable-next-line
  history: PropTypes.object.isRequired,
  currentUserId: PropTypes.string,
};

ProductDetail.defaultProps = {
  match: 'object',
  currentUserId: '',
};
