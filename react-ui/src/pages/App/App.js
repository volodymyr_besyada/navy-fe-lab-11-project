import React, { useState } from 'react';
import './App.css';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AuthService from '../../services/AuthService';
import { storeUser } from '../../store/actions/userActions';
import AllProducts from '../../containers/AllProducts/AllProducts';
import Form from '../../components/SearchForm/Form';
import Spinner from '../../components/Spinner/Spinner';

function App({ user, storeUserAction }) {
  const [loading, setLoading] = useState(false);
  const Auth = new AuthService();

  // check if there is a token in localStorage and if there's a user in store
  if (Auth.loggedIn() && !user.email && !loading) {
    if (!loading) {
      setLoading(true);
    }
    Auth.getLoggedUser().then((res) => {
      storeUserAction(res.user);
      setLoading(false);
    });
  }

  return (
    <div className="App">
      <Spinner loading={loading} />
      <Container>
        <div>
          <Form />
        </div>

        <div>
          <AllProducts />
        </div>

      </Container>
    </div>
  );
}

App.propTypes = {
  storeUserAction: PropTypes.func.isRequired,
  // eslint-disable-next-line
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, { storeUserAction: storeUser })(App);
