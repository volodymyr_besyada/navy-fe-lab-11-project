import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';

import ChatListItem from '../../components/chat/ChatListItem';
import {
  fetchChatList,
  removeChatListFromStore,
} from '../../store/actions/chatListActions';

import Spinner from '../../components/Spinner/Spinner';

const ChatsList = (props) => {
  const { chats, currentUser, dispatch } = props;

  useEffect(() => {
    /* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
    // if user is login fetch his list of chats
    if (Object.keys(currentUser).length) {
      dispatch(fetchChatList(currentUser._id));
    } else {
      dispatch(removeChatListFromStore());
    }
  }, [dispatch, currentUser]);

  const chatList = chats.map((chat) => {
    const currentUserRole = chat.shopper._id === currentUser._id ? chat.owner : chat.shopper;
    return (
      <ChatListItem
        key={chat._id}
        chatLink={`/chat/${chat.productId}/${chat.owner._id}/${chat.shopper._id}`}
        avatar={currentUserRole.profilePhoto}
        userName={currentUserRole.firstName}
      />
    );
  });

  return (
    <Container>
      <List component="nav">
        <Spinner loading={!chatList.length} />
        {chatList.length > 0 ? chatList : ''}
      </List>
    </Container>
  );
};

ChatsList.propTypes = {
  chats: PropTypes.instanceOf(Array),
  currentUser: PropTypes.instanceOf(Object),
  dispatch: PropTypes.func.isRequired,
};

ChatsList.defaultProps = {
  chats: [],
  currentUser: {},
};

const mapStateToProps = (state) => ({
  chats: state.chatsList.chats,
  currentUser: state.user.user,
});

export default connect(mapStateToProps)(ChatsList);
