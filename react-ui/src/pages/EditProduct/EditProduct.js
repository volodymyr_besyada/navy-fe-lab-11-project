import React, { useState } from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import PropTypes from 'prop-types';
import classes from './EditProduct.module.scss';
import AuthService from '../../services/AuthService';
import Spinner from '../../components/Spinner/Spinner';
import filters from '../../api/filters';

const EditProduct = (props) => {
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [longDescription, setLongDescription] = useState('');
  const [price, setPrice] = useState('');
  const Auth = new AuthService();
  const { match } = props;
  const { params } = match;
  const { id: productID } = params;
  const { history: { push } } = props;
  let result;
  const [selectedImage, setSelectedImage] = useState('no image chosen');

  // Load product values.
  if (!name) {
    if (loading === false) {
      setLoading(true);
    }
    axios.get(`/api/product/${productID}`)
      .then((resp) => {
        setLoading(false);
        result = resp.data;
        setName(result.productName);
        setDescription(result.description);
        setLongDescription(result.longDescription);
        setPrice(result.price);
      });
  }

  const setBase64 = (e) => {
    if (e.nativeEvent.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(e.nativeEvent.target.files[0]);

      reader.onload = () => {
        setSelectedImage(reader.result);
      };
    }
  };

  const updateHandler = (e) => {
    e.preventDefault();
    const productName = e.target.productName.value;
    const newDescription = e.target.description.value;
    const newLongDescription = e.target.longDescription.value;
    const category = e.target.category.value;
    const newPrice = +e.target.price.value;
    const { id } = Auth.getProfile();
    const updateData = {
      productName,
      description: newDescription,
      longDescription: newLongDescription,
      price: newPrice,
      author: id,
      category,
      productID,
    };
    const updateProperties = Object.keys(updateData);
    const validUpdateData = {};

    updateProperties.forEach((prop) => {
      if (Object.prototype.hasOwnProperty.call(updateData, prop) && updateData[prop]) {
        validUpdateData[prop] = updateData[prop];
      }
    });

    if (selectedImage !== 'no image chosen') {
      validUpdateData.productPhoto = selectedImage;
    }

    // check that we have more than product and user ID in the object
    if (Object.entries(validUpdateData).length > 2) {
      setLoading(true);
      axios.put('/api/update_product', validUpdateData)
        .then((response) => {
          setLoading(false);
          if (response.data === 'Product updated') {
            push(`/product/${productID}`);
          }
        })
        .catch((err) => {
          setLoading(false);
          // eslint-disable-next-line
          console.log(err);
          // eslint-disable-next-line
          alert('it didn\'t go well, try again later please');
        });
    }
    return true;
  };

  return (
    <Container className={classes.AddProductContainer}>
      <Spinner loading={loading} />
      <form className="form" onSubmit={updateHandler}>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <div className={classes.InputWrapper}>
              <p>Product name:</p>
              <TextField
                name="productName"
                fullWidth
                id="name"
                label={name}
                margin="dense"
                variant="outlined"
              />
            </div>
            <div className={classes.InputWrapper}>
              <p>Description:</p>
              <TextField
                id="outlined-dense-multiline"
                name="description"
                label={description}
                margin="dense"
                fullWidth
                variant="outlined"
                multiline
                rowsMax="6"
              />
            </div>
            <div className={classes.InputWrapper}>
              <p>Long description:</p>
              <TextField
                id="outlined-dense-multiline"
                name="longDescription"
                label={longDescription}
                margin="dense"
                fullWidth
                variant="outlined"
                multiline
                rowsMax="6"
              />
            </div>
            <div className={classes.InputWrapper}>
              <p>Category:</p>
              <FormControl component="fieldset">
                <RadioGroup aria-label="category" name="category">
                  {filters.categories.map((item) => (
                    <FormControlLabel
                      key={item.id}
                      value={item.value}
                      control={<Radio />}
                      label={item.title}
                    />
                  ))}
                </RadioGroup>
              </FormControl>
            </div>
            <div className={classes.InputWrapper}>
              <input
                accept="image/*"
                className={classes.input}
                style={{ display: 'none' }}
                name="file"
                id="file"
                type="file"
                onChange={setBase64}
                multiple={false}
              />
              <label htmlFor="file">
                <Button variant="outlined" htmlFor="file" component="span" className={classes.button}>
                  {'Upload New Photo'}
                </Button>
              </label>
              {
                (selectedImage !== 'no image chosen')
                  ? <img src={selectedImage} alt="" style={{ width: '50px', height: '50px', display: 'block' }} />
                  : <p>{selectedImage}</p>
              }
            </div>
            <div className={classes.InputWrapper}>
              <p>Rent/day:</p>
              <TextField
                id="outlined-dense-multiline"
                name="price"
                label={price}
                margin="dense"
                fullWidth
                variant="outlined"
                multiline
                rowsMax="6"
              />
            </div>
          </Grid>
        </Grid>
        <Button
          variant="contained"
          color="primary"
          type="submit"
        >
          Update
        </Button>
      </form>
    </Container>
  );
};


export default EditProduct;

EditProduct.defaultProps = {
  match: 'foo',
};

EditProduct.propTypes = {
  // eslint-disable-next-line
  history: PropTypes.object.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
};
