import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import classes from './About.module.scss';


const About = () => (
  <Container className={classes.About}>
    <Grid item xs={12} justify="center">
      <h1 className={classes.Heading}>About us</h1>
      <p>
We all have something that could be valuable to someone else. So
          why not share it? Help a neighbor by providing an economical
          alternative to buying, establish a new connection. Is there
          something you want to share with the world? Our app can help you.
      </p>
      <p>
          Here you can find many things that do not need to be bought.
          It is enough to find the right product, check its availability and
          reserve.
          If necessary, you can contact the owner of the product using our
          online chat, which is available on the detailed page of each
          product.
          Choose, book, use
      </p>
      <h3 className={classes.SubHead}>Plans for future:</h3>
      <ul>
        <li>Implement payment card support</li>
        <li>Implement captcha for registration an login</li>
        <li>Login with social networks (google, facebook)</li>
        <li>Top products block</li>
        <li>List of viewed products on profile page</li>
        <li>Improve security</li>
      </ul>

      <h3 className={classes.SubHead}>Our team</h3>
      <ul>
        <li>
          <b>Project Coordinator:</b>
          {' '}
Volodymyr Besyada
        </li>
        <li>
          <b>FE developer:</b>
          {' '}
Oleh Myltovych
        </li>
        <li>
          <b>FE developer:</b>
          {' '}
Vyacheslav Surminskiy
        </li>
        <li>
          <b>FE developer:</b>
          {' '}
Victor Lysechko
        </li>
        <li>
          <b>FE developer:</b>
          {' '}
Artem Ostavnenko
        </li>
      </ul>

    </Grid>
  </Container>
);


export default About;
