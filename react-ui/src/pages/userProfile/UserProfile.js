import React, { useState, useRef } from 'react';
import './UserProfile.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { storeUser } from '../../store/actions/userActions';
import AuthService from '../../services/AuthService';
import AllProducts from '../../containers/AllProducts/AllProducts';
import Spinner from '../../components/Spinner/Spinner';

const useStyles = makeStyles((theme) => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.primary.main,
    },
  },
  paper: {
    backgroundColor: theme.palette.common.white,
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  updateUserForm: {
    width: '100%',
    padding: theme.spacing(3, 3, 0, 3),
  },
  formItem: {
    marginBottom: theme.spacing(2),
    color: 'black',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  profileUserProductsHead: {
    display: 'flex',
    color: 'white',
    marginBottom: theme.spacing(3),
  },
  newProductLink: {
    marginLeft: 'auto',
    textDecoration: 'none',
  },
  newProductBtn: {
    color: '#3f51b5',
    height: '100%',
    backgroundColor: '#fff',
    '&:hover': {
      color: '#fff',
      backgroundColor: '#3f51b5',
      borderWidth: '2px',
      borderStyle: 'solid',
      borderColor: '#fff',
    },
  },
}));


const UserProfile = ({ user, storeUserAction, history }) => {
  const Auth = new AuthService();
  const classes = useStyles();
  const [userUpdate, setUserUpdate] = useState({});
  const [statusInfo, setStatusInfo] = useState('Up To Date');
  const [loading, setLoading] = useState(false);
  const formRef = useRef('form');
  const [selectedImage, setSelectedImage] = useState('no image chosen');

  if (!Auth.loggedIn()) {
    history.push('/');
  }

  const handleChange = (e) => {
    const updateProperty = e.target.name;
    const newValue = e.target.value;

    setUserUpdate({
      ...userUpdate,
      [updateProperty]: newValue,
    });
  };

  const setBase64 = (e) => {
    if (e.nativeEvent.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(e.nativeEvent.target.files[0]);

      reader.onload = () => {
        setSelectedImage(reader.result);
      };
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const updateData = {};
    const userUpdateProperties = Object.keys(userUpdate);

    if (userUpdate.phone && userUpdate.phone.length < 10) {
      return setStatusInfo('phone number cannot be shorter than 10 characters, check it please');
    }

    userUpdateProperties.forEach((prop) => {
      if (Object.prototype.hasOwnProperty.call(userUpdate, prop) && userUpdate[prop]) {
        updateData[prop] = userUpdate[prop];
      }
    });

    if (selectedImage !== 'no image chosen') {
      updateData.profilePhoto = selectedImage;
    }

    if (Object.entries(updateData).length !== 0) {
      updateData.userId = user._id;

      setLoading(true);
      setStatusInfo('Loading');
      axios.put('/api/update_user', updateData)
        .then((result) => {
          if (typeof result.data === 'string') {
            setStatusInfo(result.data);
          } else {
            storeUserAction(result.data);
            setStatusInfo('Up To Date');
            setSelectedImage('no image chosen');
          }
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
          setStatusInfo('Something went wrong, try later please');
          // eslint-disable-next-line
          console.log(err);
        });
    }
    return true;
  };

  return (
    <Container component="main" maxWidth="lg">
      <Spinner loading={loading} />
      <CssBaseline />
      <div className={classes.paper}>
        <Grid container>
          <Grid item md={5} xs={12}>
            {
              user.profilePhoto === 'none'
                ? (
                  <Typography component="h3" variant="h3" style={{ marginTop: '20px', textAlign: 'center' }}>
                Upload Your Photo, Please:)
                  </Typography>
                )
                : <img src={user.profilePhoto} alt="avatar" style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
            }
          </Grid>
          <Grid item md={7} xs={12}>
            <Typography component="h3" variant="h3" style={{ marginTop: '20px', textAlign: 'center' }}>
              My Profile Info
            </Typography>

            <ValidatorForm
              ref={formRef}
              onSubmit={handleSubmit}
              className={classes.updateUserForm}
            >
              <p style={{ textAlign: 'center' }}>
(
                {statusInfo}
)
              </p>
              <Grid container spacing={2} style={{ padding: '0 50px' }}>
                <TextField
                  className={classes.formItem}
                  autoComplete="fname"
                  name="firstName"
                  fullWidth
                  id="firstName"
                  label={`First Name: ${user.firstName || 'loading...'}`}
                  onChange={(e) => handleChange(e)}
                />
                <TextField
                  className={classes.formItem}
                  fullWidth
                  id="lastName"
                  label={`Last Name: ${user.lastName || 'loading...'}`}
                  name="lastName"
                  autoComplete="lname"
                  onChange={(e) => handleChange(e)}
                />
                <TextValidator
                  className={classes.formItem}
                  fullWidth
                  label={`Email Address: ${user.email || 'loading...'}`}
                  onChange={handleChange}
                  name="email"
                  value={userUpdate.email || ''}
                  validators={['isEmail']}
                  errorMessages={['a valid email is xxxx@xx.xx']}
                />
                <TextValidator
                  className={classes.formItem}
                  fullWidth
                  label={`Phone Number: ${user.phone || 'loading...'}`}
                  onChange={handleChange}
                  name="phone"
                  value={userUpdate.phone || ''}
                  validators={['maxStringLength:15', 'matchRegexp:^[0-9]*[0-9]$']}
                  errorMessages={['The number is too long to be real, check it please', 'Only numbers are accepted']}
                />
              </Grid>
              <Grid container spacing={2} style={{ padding: '0 50px' }}>
                <input
                  accept="image/*"
                  className={classes.input}
                  style={{ display: 'none' }}
                  name="file"
                  id="file"
                  type="file"
                  onChange={setBase64}
                  multiple={false}
                />
                <label htmlFor="file">
                  <Button variant="outlined" htmlFor="file" component="span" className={classes.button}>
                    {'Upload New Photo'}
                  </Button>
                </label>
                {
                  (selectedImage !== 'no image chosen')
                    ? <img src={selectedImage} alt="" style={{ width: '50px', height: '50px', display: 'block' }} />
                    : <p>{selectedImage}</p>
                }
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Update My Info
                </Button>
              </Grid>
            </ValidatorForm>
          </Grid>
        </Grid>
      </div>
      <div>
        <div className={classes.profileUserProductsHead}>
          <Typography component="span" variant="h3">
            My Goods
          </Typography>
          <Link to="/add-product/" className={classes.newProductLink}>
            <Button
              variant="contained"
              color="primary"
              className={classes.newProductBtn}
            >
              Add A New Product
            </Button>
          </Link>
        </div>
        {
          (user._id)
            ? <AllProducts userPage userId={user._id} />
            : ''
        }
      </div>
    </Container>
  );
};

UserProfile.propTypes = {
  // eslint-disable-next-line
  user: PropTypes.object.isRequired,
  // eslint-disable-next-line
  history : PropTypes.object.isRequired,
  storeUserAction: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, { storeUserAction: storeUser })(UserProfile);
