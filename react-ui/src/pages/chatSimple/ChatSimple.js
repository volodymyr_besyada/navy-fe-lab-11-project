import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

import ChatForm from '../../components/chat/ChatForm';
import ChatHistory from '../../components/chat/ChatHistory';

import {
  fetchChatSimple,
  removeChatSimpleFromStore,
  getNewChatMessage,
} from '../../store/actions/chatSimpleActions';

import { createSocketConnection } from '../../store/actions/socketActions';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    borderLeft: `1px solid ${theme.palette.action.hover}`,
    borderRight: `1px solid ${theme.palette.action.hover}`,
  },
}));

const ChatSimple = (props) => {
  const {
    messages,
    currentUser,
    dispatch,
    match,
    socket,
  } = props;
  const { productId, ownerId, shopperId } = match.params;

  /* eslint-disable */
  useEffect(() => {
    if (currentUser._id) {
      dispatch(createSocketConnection(currentUser._id));
    }
  }, [currentUser]);
  /* eslint-enable */

  useEffect(() => {
    // if user is login fetch his chat history with another user
    if (Object.keys(currentUser).length) {
      dispatch(fetchChatSimple(productId, ownerId, shopperId));
    }
  }, [currentUser, dispatch, productId, ownerId, shopperId]);

  /* eslint-disable */
  useEffect(() => {
    if (socket && !socket.hasListeners('receive new message')) {
      socket.on('receive new message', (newMsg) => {
        dispatch(getNewChatMessage(newMsg));
      });
    }
  }, [socket]);
  /* eslint-enable */

  /* eslint-disable */
  useEffect( () => {
    return () => { dispatch(removeChatSimpleFromStore()) };
  }, []);
  /* eslint-enable */

  const classes = useStyles();
  return (
    <Container style={{ height: 'calc(100vh - 75px)' }}>
      <div className={classes.root}>
        <ChatHistory messages={messages.chatHistory} />
        <ChatForm />
      </div>
    </Container>
  );
};

ChatSimple.propTypes = {
  messages: PropTypes.instanceOf(Object),
  currentUser: PropTypes.instanceOf(Object),
  socket: PropTypes.instanceOf(Object),
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      productId: PropTypes.string,
      ownerId: PropTypes.string,
      shopperId: PropTypes.string,
    }),
  }),
};

ChatSimple.defaultProps = {
  messages: {},
  currentUser: {},
  socket: {},
  match: PropTypes.shape({
    params: PropTypes.shape({
      productId: '',
      ownerId: '',
      shopperId: '',
    }),
  }),
};

const mapStateToProps = (state) => ({
  messages: state.chatMessages.messages,
  currentUser: state.user.user,
  socket: state.socket.socket,
});

export default connect(mapStateToProps)(ChatSimple);
