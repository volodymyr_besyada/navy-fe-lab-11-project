import React, { useState } from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import PropTypes from 'prop-types';
import classes from './AddProduct.module.scss';
import AuthService from '../../services/AuthService';
import Spinner from '../../components/Spinner/Spinner';
import filters from '../../api/filters';

const AddProduct = (props) => {
  const [loading, setLoading] = useState(false);
  const Auth = new AuthService();
  const { history: { push } } = props;
  const [selectedImage, setSelectedImage] = useState('no image chosen');

  const setBase64 = (e) => {
    if (e.nativeEvent.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(e.nativeEvent.target.files[0]);

      reader.onload = () => {
        setSelectedImage(reader.result);
      };
    }
  };

  const submitHandler = (e) => {
    e.preventDefault();
    const productName = e.target.productName.value;
    const description = e.target.description.value;
    const longDescription = e.target.longDescription.value;
    const category = e.target.category.value;
    const price = +e.target.price.value;
    const { id } = Auth.getProfile();


    setLoading(true);
    axios.post('/api/add_product', {
      productName,
      productPhoto: selectedImage,
      description,
      longDescription,
      category,
      price,
      author: id,
    })
      .then((response) => {
        setLoading(false);
        if (response.data === 'Product was added!') {
          push('/user_profile');
        }
      })
      .catch((err) => {
        setLoading(false);
        // eslint-disable-next-line
        console.log(err);
        // eslint-disable-next-line
        alert('it didn\'t go well, try again later please');
      });
    return true;
  };

  if (!Auth.loggedIn()) {
    push('/login');
  }

  return (
    <Container className={classes.AddProductContainer}>
      <Spinner loading={loading} />
      <form className="form" onSubmit={submitHandler}>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <div className={classes.InputWrapper}>
              <p>Product name:</p>
              <TextField
                name="productName"
                fullWidth
                id="name"
                label="Product name"
                margin="dense"
                variant="outlined"
              />
            </div>
            <div className={classes.InputWrapper}>
              <p>Description:</p>
              <TextField
                id="outlined-dense-multiline"
                name="description"
                label="Add description"
                margin="dense"
                fullWidth
                variant="outlined"
                multiline
                rowsMax="6"
              />
            </div>
            <div className={classes.InputWrapper}>
              <p>Long description:</p>
              <TextField
                id="outlined-dense-multiline"
                name="longDescription"
                label="Long description"
                margin="dense"
                fullWidth
                variant="outlined"
                multiline
                rowsMax="6"
              />
            </div>
            <div className={classes.InputWrapper}>
              <p>Category:</p>
              <FormControl component="fieldset">
                <RadioGroup aria-label="category" name="category">
                  {filters.categories.map((item) => (
                    <FormControlLabel
                      key={item.id}
                      value={item.value}
                      control={<Radio />}
                      label={item.title}
                    />
                  ))}
                </RadioGroup>
              </FormControl>
            </div>
            <div className={classes.InputWrapper}>
              <input
                accept="image/*"
                className={classes.input}
                style={{ display: 'none' }}
                name="file"
                id="file"
                type="file"
                onChange={setBase64}
                multiple={false}
              />
              <label htmlFor="file">
                <Button variant="outlined" htmlFor="file" component="span" className={classes.button}>
                  {'Upload New Photo'}
                </Button>
              </label>
              {
                (selectedImage !== 'no image chosen')
                  ? <img src={selectedImage} alt="" style={{ width: '50px', height: '50px', display: 'block' }} />
                  : <p>{selectedImage}</p>
              }
            </div>
            <div className={classes.InputWrapper}>
              <p>Rent/day:</p>
              <TextField
                id="outlined-dense-multiline"
                name="price"
                label="Price $"
                margin="dense"
                fullWidth
                variant="outlined"
                multiline
                rowsMax="6"
              />
            </div>
          </Grid>
        </Grid>
        <Button
          variant="contained"
          color="primary"
          type="submit"
        >
          Add product
        </Button>
      </form>

    </Container>
  );
};


export default AddProduct;


AddProduct.propTypes = {
  // eslint-disable-next-line
  history: PropTypes.object.isRequired,
};
