import React, { useState, useRef } from 'react';
import axios from 'axios';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import PropTypes from 'prop-types';
import AuthService from '../../services/AuthService';
import Spinner from '../../components/Spinner/Spinner';

const useStyles = makeStyles((theme) => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.primary.main,
    },
  },
  paper: {
    backgroundColor: theme.palette.common.white,
    borderRadius: 8,
    marginTop: theme.spacing(8),
    padding: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignUp = ({ history }) => {
  const Auth = new AuthService();
  const classes = useStyles();
  const [userInput, setUserInput] = useState({});
  const [loading, setLoading] = useState(false);
  const formRef = useRef('form');

  const handleChange = (e) => {
    const property = e.target.name;
    const { value } = e.target;
    setUserInput({
      ...userInput,
      [property]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (loading === false) {
      setLoading(true);
    }
    axios.post('/api/add_user', userInput)
      .then((response) => {
        if (response.data === 'New user has been created!') {
          setLoading(false);
          history.push('/login');
        }
      })
      .catch((error) => {
        // eslint-disable-next-line
        console.log(error);
        setLoading(false);
      });
    return true;
  };

  if (Auth.loggedIn()) {
    history.push('/');
  }

  ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
    if (value !== userInput.password) {
      return false;
    }
    return true;
  });

  return (
    <Container component="main" maxWidth="sm">
      <Spinner loading={loading} />
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <ValidatorForm
          ref={formRef}
          onSubmit={handleSubmit}
          className={classes.form}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextValidator
                variant="outlined"
                fullWidth
                label="First Name"
                onChange={handleChange}
                name="firstName"
                value={userInput.firstName || ''}
                validators={['required']}
                errorMessages={['this field is required']}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextValidator
                fullWidth
                variant="outlined"
                label="Last Name"
                onChange={handleChange}
                name="lastName"
                value={userInput.lastName || ''}
                validators={['required']}
                errorMessages={['this field is required']}
              />
            </Grid>
            <Grid item xs={12}>
              <TextValidator
                variant="outlined"
                label="Email Address"
                onChange={handleChange}
                name="email"
                fullWidth
                value={userInput.email || ''}
                validators={['required', 'isEmail']}
                errorMessages={['this field is required', 'valid email is xxxx@xx.xx']}
              />
            </Grid>
            <Grid item xs={12}>
              <TextValidator
                variant="outlined"
                label="Phone"
                onChange={handleChange}
                name="phone"
                fullWidth
                value={userInput.phone || ''}
                validators={['required', 'matchRegexp:^[0-9]*[0-9]$', 'minStringLength:10', 'maxStringLength:15']}
                errorMessages={['this field is required', 'Only numbers are accepted', 'The number is too short to be real', 'The number is too long to be real']}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextValidator
                fullWidth
                variant="outlined"
                label="Password"
                onChange={handleChange}
                name="password"
                type="password"
                validators={['required']}
                errorMessages={['this field is required']}
                value={userInput.password || ''}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextValidator
                fullWidth
                variant="outlined"
                label="Repeat password"
                onChange={handleChange}
                name="repeatPassword"
                type="password"
                validators={['required', 'isPasswordMatch']}
                errorMessages={['this field is required', 'password mismatch']}
                value={userInput.repeatPassword || ''}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
                Sign Up
          </Button>
        </ValidatorForm>
      </div>
    </Container>
  );
};

SignUp.propTypes = {
  // eslint-disable-next-line
  history: PropTypes.object.isRequired,
};

export default SignUp;
