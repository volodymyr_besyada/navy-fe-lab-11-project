import React, { useState, useRef } from 'react';
import './Login.css';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import AuthService from '../../services/AuthService';
import { storeUser } from '../../store/actions/userActions';
import Spinner from '../../components/Spinner/Spinner';

const useStyles = makeStyles((theme) => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.primary.main,
    },
  },
  paper: {
    backgroundColor: theme.palette.common.white,
    borderRadius: 8,
    marginTop: theme.spacing(8),
    padding: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = ({ storeUserAction, history }) => {
  const Auth = new AuthService();
  const classes = useStyles();
  const formRef = useRef('form');

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [alert, setAlert] = useState('');
  const [loading, setLoading] = useState(false);

  function handleSubmit(e) {
    e.preventDefault();
    if (loading === false) {
      setLoading(true);
    }
    Auth.login(email, password)
      .then((res) => {
        storeUserAction(res.user);
        setLoading(false);
        history.push('/user_profile');
      })
      .catch((err) => {
        setAlert('There is no user with these credentials');
        // eslint-disable-next-line
        console.log(err);
        setLoading(false);
      });
  }

  function handleChange(e) {
    return (e.target.name === 'email') ? setEmail(e.target.value) : setPassword(e.target.value);
  }

  return (
    <Container component="main" maxWidth="sm">
      <Spinner loading={loading} />
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
            Log in
        </Typography>
        <ValidatorForm
          ref={formRef}
          onSubmit={handleSubmit}
          className={classes.form}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextValidator
                variant="outlined"
                label="Email Address"
                onChange={handleChange}
                name="email"
                fullWidth
                value={email}
                validators={['required', 'isEmail']}
                errorMessages={['this field is required', 'valid email is xxxx@xx.xx']}
              />
            </Grid>
            <Grid item xs={12}>
              <TextValidator
                fullWidth
                variant="outlined"
                label="Password"
                onChange={handleChange}
                name="password"
                type="password"
                validators={['required']}
                errorMessages={['this field is required']}
                value={password}
              />
            </Grid>
          </Grid>
          <p>{alert}</p>
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            type="submit"
          >
              Submit
          </Button>
        </ValidatorForm>
      </div>
    </Container>
  );
};

Login.propTypes = {
  storeUserAction: PropTypes.func.isRequired,
  // eslint-disable-next-line
  user: PropTypes.object.isRequired,
  // eslint-disable-next-line
  history: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, { storeUserAction: storeUser })(Login);
