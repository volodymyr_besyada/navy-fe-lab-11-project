import { createSelector } from 'reselect';

const getProductsSelector = (state) => state.products.items;
const getSortKeySelector = (state) => state.products.sortKey;

const sortSelector = createSelector(
  getProductsSelector,
  getSortKeySelector,
  (products, sortBy) => {
    switch (sortBy) {
      case 'asc':
        return products
          .slice()
          .sort((a, b) => a.productName
            .toLowerCase()
            .localeCompare(b.productName.toLowerCase()));
      case 'desc':
        return products
          .slice()
          .sort((a, b) => b.productName
            .toLowerCase()
            .localeCompare(a.productName.toLowerCase()));
      case 'newDate':
        return products
          .slice()
          .sort((a, b) => new Date(b.date) - new Date(a.date));
      case 'oldDate':
        return products
          .slice()
          .sort((a, b) => new Date(a.date) - new Date(b.date));
      case 'lowPrice':
        return products
          .slice()
          .sort((a, b) => a.price - b.price);
      case 'higherPrice':
        return products
          .slice()
          .sort((a, b) => b.price - a.price);
      default:
        return products;
    }
  },
);

export default sortSelector;
