import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchProducts } from '../../store/actions/products';
import ProductItem from '../../components/ProductItem/ProductItem';
import Pagination from '../../components/Pagination/Pagination';
import SortData from '../../components/SortData/SortData';
import ZeroResult from '../../components/SearchForm/ZeroResult';
import sortSelector from './selector';
import './AllProducts.css';
import Spinner from '../../components/Spinner/Spinner';

const AllProducts = (props) => {
  useEffect(() => {
    const { fetchProducts: getProducts, userPage, userId } = props;

    // it seems we had been sending two requests for home page products.
    // one here and the other one from Form.js of SearchForm
    /* getProducts() */
    if (userPage) {
      getProducts(userId);
    }
  }, []);

  const { error, loading, products } = props;

  const initialItemsPerPage = 12;
  const dataLength = products.length;
  const [currentPage, setCurrentPage] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(initialItemsPerPage);
  const indexOfLastItem = (currentPage + 1) * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;

  const handleChangePage = (event, newPage) => setCurrentPage(newPage);
  const handleChangeRowsPerPage = (event) => {
    setItemsPerPage(+event.target.value);
    setCurrentPage(0);
  };
  const paginate = Array.from({ length: Math.ceil(dataLength / initialItemsPerPage) })
    .map((k, v) => (v + 1) * initialItemsPerPage);

  let productList;

  if (error) {
    productList = error.message;
    return productList;
  }

  if (products.length) {
    productList = products
      .slice(indexOfFirstItem, indexOfLastItem)
      .map((product) => (
        <Grid
          item
          xs={12}
          md={6}
          lg={4}
          className="ProductItemBlock"
          key={product._id}
        >
          <ProductItem
            id={product._id}
            title={product.productName}
            description={product.description}
            price={product.price}
            category={product.category}
            photo={product.productPhoto}
          />
        </Grid>
      ));
  }

  if (loading) {
    return (
      <Spinner loading={loading} />
    );
  }

  return (
    <div className="TopProducts">
      {products.length > 0 && <SortData />}
      <Grid container spacing={2}>
        {products.length > 0 ? productList : <ZeroResult />}
      </Grid>
      {products.length > 0 && (
      <Pagination
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        length={dataLength}
        currentPage={currentPage}
        itemsPerPage={itemsPerPage}
        paginate={paginate}
      />
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  products: sortSelector(state) || [],
  user: state.user.user,
  loading: state.products.loading,
  error: state.products.error,
  sortKey: state.products.sortBy,
  page: state.products.page,
});

const mapDispatchToProps = (dispatch) => ({
  fetchProducts: (userId) => dispatch(fetchProducts(userId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AllProducts);

AllProducts.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
  error: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
};
