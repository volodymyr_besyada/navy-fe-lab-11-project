import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import axios from 'axios';
import App from '../pages/App/App';
import Header from '../components/Header/Header';
import ProductDetail from '../pages/ProductDetail/ProductDetail';
import AddProduct from '../pages/AddProduct/AddProduct';
import Login from '../pages/login/Login';
import UserProfile from '../pages/userProfile/UserProfile';
import SignUp from '../pages/signUp/SignUp';
import ChatsList from '../pages/chatList/ChatList';
import ChatSimple from '../pages/chatSimple/ChatSimple';
import EditProduct from '../pages/EditProduct/EditProduct';
import About from '../pages/About/About';

function Users() {
  // eslint-disable-next-line
  axios.get('/api/users').then((resp) => console.log(resp.data));
  return (
    <div>
      <h3>
The users Section
        (test page)
      </h3>
      <p>
        the request to get all users is sent every time this page is being opened
        <b> (check all users from the database in the console)</b>
      </p>
    </div>
  );
}

function AppRouter() {
  return (
    <Router>
      <div>
        <Header />
        <Route exact path="/" component={App} />
        <Route path="/about/" component={About} />
        <Route path="/users/" component={Users} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/user_profile" component={UserProfile} />
        <Route exact path="/sign_up" component={SignUp} />
        <Route path="/product/:id" component={ProductDetail} />
        <Route path="/add-product/" component={AddProduct} />
        <Route path="/myChats/" component={ChatsList} />
        <Route path="/chat/:productId/:ownerId/:shopperId" component={ChatSimple} />
        <Route path="/edit-product/:id" component={EditProduct} />
      </div>
    </Router>
  );
}

export default AppRouter;
