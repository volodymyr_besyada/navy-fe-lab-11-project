import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { sortByKey } from '../../store/actions/products';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-end',
  },
  formControl: {
    marginBottom: theme.spacing(1),
    minWidth: 120,
  },
}));

const SortData = (props) => {
  const classes = useStyles();
  const [sortKey, setSortKey] = React.useState('');

  const handleChange = (e) => {
    setSortKey(e.target.value);
    props.sortByKey(e.target.value);
  };

  return (
    <form className={classes.root} autoComplete="off">
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="select">SORT BY</InputLabel>
        <Select
          id="select"
          inputProps={{ value: sortKey, id: 'select' }}
          value={sortKey}
          onChange={handleChange}
        >
          <MenuItem value="asc">TITLE A-Z</MenuItem>
          <MenuItem value="desc">TITLE Z-A</MenuItem>
          <MenuItem value="lowPrice">LOW PRICE</MenuItem>
          <MenuItem value="higherPrice">HIGHER PRICE</MenuItem>
          <MenuItem value="newDate">DATE NEW</MenuItem>
          <MenuItem value="oldDate">DATE OLD</MenuItem>
        </Select>
      </FormControl>
    </form>
  );
};

const mapDispatchToProps = (dispatch) => ({
  sortByKey: (key) => dispatch(sortByKey(key)),
});

export default connect(
  null,
  mapDispatchToProps,
)(SortData);

SortData.propTypes = {
  sortByKey: PropTypes.func.isRequired,
};
