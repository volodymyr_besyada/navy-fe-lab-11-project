import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));

const SearchButton = ({ handlerClick, resetRender }) => {
  const classes = useStyles();

  return (
    <div>
      <Button
        variant="contained"
        size="large"
        color="primary"
        className={classes.button}
        disabled={!resetRender}
        onClick={handlerClick}
      >
        SEARCH
      </Button>
    </div>
  );
};

export default SearchButton;

SearchButton.propTypes = {
  handlerClick: PropTypes.func.isRequired,
  resetRender: PropTypes.bool.isRequired,
};
