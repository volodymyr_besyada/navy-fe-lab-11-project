import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    width: '100%',
    height: '70vh',
    position: 'relative',
  },
});

const ZeroResult = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography
        variant="h3"
        component="h1"
        style={{
          top: '50%',
          right: '50%',
          transform: 'translate(50%,-50%)',
          position: 'absolute',
        }}
      >
        Sorry, on your request nothing found...
      </Typography>
    </div>
  );
};

export default ZeroResult;
