import React, { useState, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import {
  FormControl,
  InputLabel,
  Input,
  Grid,
  Select,
  MenuItem,
  InputAdornment,
  Button,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import SearchButton from './SearchButton';
import filters from '../../api/filters';
import { filterProducts, fetchProducts } from '../../store/actions/products';

const useStyles = makeStyles(() => ({
  root: {
    paddingTop: 40,
    paddingBottom: 20,
  },
  input: {
    flexGrow: 1,
  },
  button: {
    flexGrow: 1,
  },
  formControl: {
    textAlign: 'left',
  },
}));

const Form = (props) => {
  const classes = useStyles();
  useEffect(() => {
    const { fetchProducts: getProducts } = props;
    getProducts();
  }, []);

  const { products } = props;
  const [category, setCategory] = useState({
    category: '',
  });
  const [searchValue, setSearchValue] = useState('');
  const [isClicked, setIsClicked] = useState(false);
  const handleSearchInputChanges = (e) => {
    setSearchValue(e.target.value);
  };

  const getNumberOfCategories = (params) => {
    const obj = {};
    params.forEach((value) => {
      if (typeof obj[value.category] === 'undefined') {
        obj[value.category] = 1;
        return;
      }
      obj[value.category] += 1;
    });
    return obj;
  };

  const getCategories = products.length > 0 ? getNumberOfCategories(products) : 0;

  const useFetchData = ({ url, headers, payload }) => {
    const [res, setRes] = useState({ data: null, error: null });

    const callToServer = useCallback(() => {
      axios
        .post(url, payload, headers)
        .then((response) => {
          setRes({ data: response.data, error: null });
          props.filterProducts(response.data);
        })
        .catch((error) => {
          setRes({ data: null, error });
        });
    }, [url, headers, payload]);
    return [res, callToServer];
  };

  const handlerReset = (e) => {
    e.preventDefault();
    setSearchValue('');
    setCategory({ category: '' });
    if (isClicked) props.fetchProducts();
    if (isClicked) setIsClicked(false);
  };

  const resetRender = !!category.category || !!searchValue;

  const [, callToServer] = useFetchData({
    url: '/api/filter',
    headers: { ContentType: 'text/plain' },
    payload: { category: category.category, searchValue },
  });

  const handlerClick = (e) => {
    e.preventDefault();
    setIsClicked(true);
    callToServer();
  };

  const handlerChange = (e) => {
    setCategory({
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={6}
      >
        <Grid item lg={6} md={6} xs={12} sm={6}>
          <FormControl className={classes.textField} fullWidth>
            <InputLabel htmlFor="input-with-icon-adornment">
              Enter Product Name
            </InputLabel>
            <Input
              id="input-with-icon-adornment"
              className={classes.input}
              value={searchValue}
              onChange={handleSearchInputChanges}
              inputProps={{ value: searchValue, id: 'input-with-icon-adornment' }}
              fullWidth
              endAdornment={(
                <InputAdornment position="end">
                  <SearchIcon />
                </InputAdornment>
)}
            />
          </FormControl>
        </Grid>
        <Grid item lg={3} md={4} xs={12} sm={6}>
          <FormControl fullWidth>
            <InputLabel htmlFor="category">Categories</InputLabel>
            <Select
              id="category"
              className={classes.formControl}
              value={category.category}
              inputProps={{ value: category.category, id: 'category' }}
              name="category"
              onChange={handlerChange}
            >
              {filters.categories.map((item) => (
                <MenuItem key={item.id} value={item.value}>
                  {item.title}
                  {' '}
                  {`(${typeof getCategories[item.value] === 'undefined' ? 0 : getCategories[item.value]})`}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item lg={2} md={2} xs={12} sm={12}>
          <SearchButton handlerClick={handlerClick} resetRender={resetRender} />
        </Grid>
      </Grid>
      {resetRender && (
        <div>
          <Button size="small" onClick={handlerReset}>
            <ClearIcon />
            <span>RESET ALL</span>
          </Button>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  products: state.products.items || [],
});

const mapDispatchToProps = (dispatch) => ({
  filterProducts: (products) => dispatch(filterProducts(products)),
  fetchProducts: () => dispatch(fetchProducts()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form);

Form.propTypes = {
  filterProducts: PropTypes.func.isRequired,
  fetchProducts: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
};
