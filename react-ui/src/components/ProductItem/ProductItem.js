import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classes from './ProductItem.module.scss';


const ProductItem = ({
  id, title, description, price, category, photo,
}) => {
  // Image placeholder.
  const imgUrl = 'http://placeimg.com/240/280/any';
  // Custom number for random image.
  const randomRequest = Math.floor(Math.random() * 100);
  // Image url.
  const imagUrlPattern = `${imgUrl}?${randomRequest}`;

  return (
    <Link to={`/product/${id}`} className={classes.ProductItem}>
      {
        (photo !== 'none')
          ? <img src={photo} alt="Product detail" />
          : <img src={imagUrlPattern} alt="Product detail" style={{ width: '100%', height: '100%' }} />
      }
      <h4>{title}</h4>
      <p>{description}</p>
      <span>{price}</span>
      <div className={classes.Category}>
        {category}
      </div>
    </Link>
  );
};

export default ProductItem;

ProductItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired,
};
