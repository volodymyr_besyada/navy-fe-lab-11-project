import React from 'react';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';
import PropTypes from 'prop-types';

const Spinner = ({ loading }) => {
  if (loading) {
    return (
      <Loader
        style={{
          position: 'fixed',
          zIndex: '2',
          left: '50%',
          top: '50%',
          transform: 'translate(-50%, -50%)',
          width: '125px',
          height: '125px',
          borderRadius: '50%',
          borderStyle: 'solid',
          borderWidth: '1px',
          borderColor: '#3f51b5',
          boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.75)',
          backgroundColor: 'white',
          textAlign: 'center',
        }}
        type="BallTriangle"
        color="#3f51b5"
        height={100}
        width={100}
      />
    );
  }
  return null;
};

Spinner.propTypes = {
  loading: PropTypes.bool.isRequired,
};

export default Spinner;
