import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import AuthService from '../../services/AuthService';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { clearUser, storeUser } from '../../store/actions/userActions';

import {
  AppBar,
  Toolbar,
  Drawer,
  Hidden,
  IconButton,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { makeStyles } from '@material-ui/core/styles';
import Spinner from '../Spinner/Spinner';

const useStyles = makeStyles((theme) => ({
  menuLogo: {
    color: '#fff',
    textDecoration: 'none',
    margin: '5px 20px 0px',
    fontSize: '30px',
    fontWeight: 'bold',
  },
  menuLink: {
    color: '#fff',
    textDecoration: 'none',
    margin: '0 20px',
    borderBottom: '2px solid transparent',
    transition: '.2s ease-in-out',
    '&:hover': {
      borderBottom: '2px solid #fff',
    },
  },
  menuLinkDrawer: {
    textDecoration: 'none',
    padding: '10px 40px',
    margin: '10px',
    fontSize: '24px',
  },
  menuLogin: {
    marginLeft: 'auto',
  },
  menuBtn: {
    display: 'none',
    [theme.breakpoints.down('sm')]: {
      display: 'block',
    },
  },
}));

function Header({ user, clearUser, storeUser }) {
  const Auth = new AuthService();
  const classes = useStyles();
  const [showDrawer, setShowDrawer] = useState(false);
  const [loading, setLoading] = useState(false);

  // check if there's a token in localStorage and
  // if there's the user's data in store
  // !loadin - not to send another request if there's already one
  if (Auth.loggedIn() && !user._id && !loading) {
    if (loading === false) {
      setLoading(true);
    }
    Auth.getLoggedUser().then((res) => {
      storeUser(res.user);
      setLoading(false);
    });
  }

  function toggleDrawer() {
    setShowDrawer(!showDrawer);
  }

  const handleLogout = () => {
    Auth.logout();// clear token from the localStorage
    clearUser();// clear user in redux store
  }

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            className={classes.menuBtn}
            onClick={toggleDrawer}
            color="inherit"
          >
            <MenuIcon />
          </IconButton>
          <Hidden smDown>
            <Link to="/" className={classes.menuLogo}>
              <img src="/logo.png" style={{maxWidth: '60px'}} alt='NAVY LOGO' />
            </Link>
            <Link to="/" className={classes.menuLink}>Home</Link>
            <Link to="/about/" className={classes.menuLink}>About</Link>
            {user._id &&
              <>
                <Link to="/add-product/" className={classes.menuLink}>Add product</Link>
                <Link to="/myChats/" className={classes.menuLink}>My Chats</Link>
              </>
            }
          </Hidden>
          {(user.email)
            ? (
              <div className={classes.menuLogin}>
                <Link to="/user_profile" className={classes.menuLink}>
                  <AccountCircle /> My Profile
                </Link>
                <Link to="/" className={classes.menuLink} onClick={handleLogout}>Log out</Link>
              </div>
            )
            :
            (
              <div className={classes.menuLogin}>
                <Link to="/sign_up" className={classes.menuLink}>Sign up</Link>
                <Link to="/login" className={classes.menuLink}>Log in</Link>
              </div>
            )
          }
        </Toolbar>
      </AppBar>
      <Hidden mdUp>
        <Drawer open={showDrawer} onClose={toggleDrawer}>
          <Link to="/" className={classes.menuLinkDrawer} onClick={toggleDrawer}>Home</Link>
          <Link to="/about/" className={classes.menuLinkDrawer} onClick={toggleDrawer}>About</Link>
          {user._id &&
            <>
              <Link to="/add-product/" className={classes.menuLinkDrawer} onClick={toggleDrawer}>Add product</Link>
              <Link to="/myChats/" className={classes.menuLinkDrawer} onClick={toggleDrawer}>My Chats</Link>
            </>
          }
        </Drawer>
      </Hidden>
      <Spinner loading={loading}/>
    </>
  );
}
Header.propTypes = {
  user: PropTypes.object.isRequired,
  clearUser: PropTypes.func.isRequired,
  storeUser: PropTypes.func.isRequired,
}
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, { clearUser, storeUser })(Header);
