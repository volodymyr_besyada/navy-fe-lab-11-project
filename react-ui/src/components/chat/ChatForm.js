/* eslint-env browser */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import { sendNewChatMessage } from '../../store/actions/chatSimpleActions';

const useStyles = makeStyles((theme) => ({
  chatForm: {
    boxSizing: 'border-box',
    display: 'flex',
    alignItems: 'flex-end',
    width: '100%',
    borderTop: `1px solid ${theme.palette.action.hover}`,
    padding: '10px',
  },
  chatInput: {
    flexGrow: '1',
    fontSize: '18px',
    resize: 'none',
    borderTopLeftRadius: '5px',
    borderTopRightRadius: '5px',
    border: 'none',
    borderBottom: '2px solid grey',
    padding: '10px',
    '&:hover': {
      background: theme.palette.action.hover,
    },
    '&:focus': {
      outline: 'none',
      background: theme.palette.action.hover,
      borderBottom: `2px solid ${theme.palette.primary.main}`,
    },
  },
}));

const ChatForm = (props) => {
  const classes = useStyles();
  const {
    messages,
    currentUser,
    dispatch,
    socket,
  } = props;
  const messageForm = React.createRef();
  const messageInput = React.createRef();

  async function sendMessage(e) {
    e.preventDefault();
    const data = new FormData(messageForm.current);
    const newMessage = data.get('message');
    if (newMessage !== '') {
      /* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
      const recipient = currentUser._id === messages.owner ? messages.shopper : messages.owner;
      dispatch(sendNewChatMessage(newMessage, currentUser._id, messages._id, socket, recipient));
      messageInput.current.value = '';
      messageInput.current.style.height = '44px';
    }
  }

  function handleSubmit(e) {
    if (e.keyCode === 13 && e.shiftKey === false) {
      sendMessage(e);
    }
  }
  return (
    <form
      onSubmit={sendMessage}
      ref={messageForm}
      className={classes.chatForm}
    >
      <TextareaAutosize
        ref={messageInput}
        onKeyDown={handleSubmit}
        className={classes.chatInput}
        placeholder="Type new message"
        resize="false"
        name="message"
      />
      <Button
        size="large"
        variant="contained"
        color="primary"
        type="submit"
        style={{ marginLeft: '10px' }}
      >
        Send
      </Button>
    </form>
  );
};

ChatForm.propTypes = {
  messages: PropTypes.instanceOf(Object),
  currentUser: PropTypes.instanceOf(Object),
  dispatch: PropTypes.func.isRequired,
  socket: PropTypes.instanceOf(Object),
};

ChatForm.defaultProps = {
  messages: {},
  currentUser: {},
  socket: {},
};

const mapStateToProps = (state) => ({
  messages: state.chatMessages.messages,
  currentUser: state.user.user,
  socket: state.socket.socket,
});

export default connect(mapStateToProps)(ChatForm);
