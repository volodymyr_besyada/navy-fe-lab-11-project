import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

const ChatListItem = (props) => {
  const { chatLink, avatar, userName } = props;
  return (
    <>
      <ListItem alignItems="center" button component={Link} to={chatLink}>
        <ListItemAvatar>
          <Avatar src={avatar} />
        </ListItemAvatar>
        <ListItemText primary={userName} />
      </ListItem>
      <Divider />
    </>
  );
};

ChatListItem.propTypes = {
  chatLink: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  userName: PropTypes.string.isRequired,
};

export default ChatListItem;
