import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  messageWrapper: {
    display: 'flex',
    marginBottom: '10px',
  },
  myMessageWrapper: {
    display: 'flex',
    marginBottom: '10px',
    flexDirection: 'row-reverse',
  },
  message: {
    maxWidth: '60%',
    padding: '15px',
    borderRadius: '5px',
    whiteSpace: 'pre-wrap',
    background: '#d9d9d9',
  },
  myMessage: {
    maxWidth: '60%',
    color: 'white',
    padding: '15px',
    borderRadius: '5px',
    whiteSpace: 'pre-wrap',
    background: 'linear-gradient(-30deg, #3F51B5, blue)',
  },
}));

const ChatHistory = (props) => {
  const classes = useStyles();

  const { messages, currentUser } = props;

  const messagesBlock = React.createRef();
  useEffect(() => {
    const domElement = messagesBlock.current;
    domElement.scrollTop = domElement.scrollHeight;
  }, [messagesBlock]);


  const messagesList = messages.map((message, ind, msgList) => {
    /* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
    const isMyMessage = message.authorId === currentUser._id;
    // message.date have format like this 2019-10-06T14:49:56.248Z
    // take this part 2019-10-06 for variable prevMsgDayCreation and currentMsgDayCreation
    let prevMsgDayCreation;
    if (ind > 0) {
      // eslint-disable-next-line
      prevMsgDayCreation = msgList[ind - 1].date.split('T')[0];
    }
    const currentMsgDayCreation = message.date.split('T')[0];
    const currentMsgTimeCreation = message.date.split('T')[1].split('').slice(0, 5);

    return (
      <React.Fragment key={message._id}>
        {(ind === 0)
          && (
          <span style={{ display: 'block', textAlign: 'center', marginBottom: '20px' }}>
            { currentMsgDayCreation }
          </span>
          )}
        {(ind > 0) && (currentMsgDayCreation !== prevMsgDayCreation)
          && (
          <span style={{ display: 'block', textAlign: 'center', marginBottom: '20px' }}>
            { currentMsgDayCreation }
          </span>
          )}
        <div className={isMyMessage ? classes.myMessageWrapper : classes.messageWrapper}>
          <p className={isMyMessage ? classes.myMessage : classes.message}>
            { message.text }
          </p>
          <span style={{ display: 'flex', alignItems: 'flex-end', padding: '0 7px' }}>
            {currentMsgTimeCreation}
          </span>
        </div>
      </React.Fragment>
    );
  });
  return (
    <div
      ref={messagesBlock}
      style={{ flexGrow: '1', overflowY: 'auto' }}
    >
      {messagesList}
    </div>
  );
};

ChatHistory.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object),
  currentUser: PropTypes.instanceOf(Object),
};

ChatHistory.defaultProps = {
  messages: [],
  currentUser: {},
};

const mapStateToProps = (state) => ({
  currentUser: state.user.user,
});

export default connect(mapStateToProps)(ChatHistory);
