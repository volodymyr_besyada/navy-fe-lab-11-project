import React from 'react';
import TablePagination from '@material-ui/core/TablePagination';
import { Grid } from '@material-ui/core';
import PropTypes from 'prop-types';

const Pagination = ({
  handleChangePage,
  handleChangeRowsPerPage,
  currentPage,
  length,
  itemsPerPage,
  paginate,
}) => (
  <Grid container direction="row" justify="center" alignItems="center">
    <TablePagination
      rowsPerPageOptions={paginate}
      component="div"
      page={currentPage}
      rowsPerPage={itemsPerPage}
      count={length}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
      backIconButtonProps={{
        'aria-label': 'previous page',
      }}
      nextIconButtonProps={{
        'aria-label': 'next page',
      }}
      labelRowsPerPage="Items per page:"
    />
  </Grid>
);

export default Pagination;

Pagination.propTypes = {
  handleChangePage: PropTypes.func.isRequired,
  handleChangeRowsPerPage: PropTypes.func.isRequired,
  paginate: PropTypes.arrayOf(PropTypes.number).isRequired,
  currentPage: PropTypes.number.isRequired,
  length: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
};
