import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import AppRouter from './appRouter/AppRouter';
import './index.css';
import store from './store/index';

// Wrap app with a Provider.
// Set global store for application.
ReactDOM.render(<Provider store={store}><AppRouter /></Provider>, document.getElementById('root'));
