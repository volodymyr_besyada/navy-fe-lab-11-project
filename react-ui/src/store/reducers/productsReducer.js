import {
  FETCH_PRODUCTS_BEGIN,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE,
  SORT_BY,
  FILTER_PRODUCTS,
} from '../actions/products';


const initialState = {
  items: [],
  loading: false,
  error: false,
  sortKey: 'asc',
};


const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS_BEGIN:
      return {
        ...state,
        loading: true,
        error: false,
      };

    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        items: action.payload.products,
      };

    case FETCH_PRODUCTS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: [],
      };

    case SORT_BY:
      return {
        ...state,
        sortKey: action.sortKey,
      };

    case FILTER_PRODUCTS:
      return {
        ...state,
        items: action.products,
      };

    default:
      return state;
  }
};


export default productsReducer;
