import {
  FETCH_PRODUCTDETAIL_BEGIN,
  FETCH_PRODUCTDETAIL_SUCCESS,
  FETCH_PRODUCTDETAIL_FAILURE,
} from '../actions/productDetail';


const initialState = {
  item: {},
  loading: false,
  error: null,
};


const productDetail = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTDETAIL_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
      };

    case FETCH_PRODUCTDETAIL_SUCCESS:
      return {
        ...state,
        loading: false,
        item: action.payload.product,
      };

    case FETCH_PRODUCTDETAIL_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        item: [],
      };

    default:
      return state;
  }
};


export default productDetail;
