import { SOCKET_CONNECTION } from '../actions/types';

const initialState = {
  socket: null,
};

const socketReducer = (state = initialState, action) => {
  switch (action.type) {
    case SOCKET_CONNECTION:
      return {
        ...state,
        socket: action.socket,
      };
    default:
      return state;
  }
};

export default socketReducer;
