import {
  FETCH_CHAT_SIMPLE_BEGIN,
  FETCH_CHAT_SIMPLE_SUCCESS,
  FETCH_CHAT_SIMPLE_FAILURE,
  REMOVE_CHAT_SIMPLE_FROM_STORE,
  NEW_CHAT_MESSAGE,
} from '../actions/types';

const initialState = {
  messages: {},
  loading: false,
  error: null,
};

/* eslint no-fallthrough: "off" */
const chatSimpleReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CHAT_SIMPLE_BEGIN:
      return {
        ...state,
        messages: {},
        loading: true,
        error: null,
      };
    case FETCH_CHAT_SIMPLE_SUCCESS:
      return {
        ...state,
        loading: false,
        messages: action.messages,
      };
    case FETCH_CHAT_SIMPLE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        messages: {},
      };
    case REMOVE_CHAT_SIMPLE_FROM_STORE:
      return {
        ...state,
        messages: {},
        loading: false,
        error: null,
      };
    case NEW_CHAT_MESSAGE:
      if (state.messages.id === action.newMessage.chatId) {
        return {
          ...state,
          messages: {
            ...state.messages,
            chatHistory: [
              ...state.messages.chatHistory, action.newMessage,
            ],
          },
        };
      }
    default:
      return state;
  }
};

export default chatSimpleReducer;
