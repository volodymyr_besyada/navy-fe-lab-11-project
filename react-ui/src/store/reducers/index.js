import { combineReducers } from 'redux';
import userReducer from './userReducer';
import productsReducer from './productsReducer';
import productDetail from './productDetail';

import chatListReducer from './chatListReducer';
import chatSimpleReducer from './chatSimpleReducer';
import socketReducer from './socketReducer';

// all the reducers will be combined here and imported in store file as one
export default combineReducers({
  user: userReducer,
  products: productsReducer,
  product: productDetail,

  chatsList: chatListReducer,
  chatMessages: chatSimpleReducer,
  socket: socketReducer,
});
