import { STORE_USER, USER_LOGOUT } from '../actions/types';

// Define default state.
const initialState = {
  user: {},
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case STORE_USER:
      return {
        ...state,
        user: action.user,
      };
    case USER_LOGOUT:
      return {
        ...state,
        user: {},
      };
    default:
      return state;
  }
};

export default userReducer;
