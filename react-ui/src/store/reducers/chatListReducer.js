import {
  FETCH_CHAT_LIST_BEGIN,
  FETCH_CHAT_LIST_SUCCESS,
  FETCH_CHAT_LIST_FAILURE,
  REMOVE_CHAT_LIST_FROM_STORE,
} from '../actions/types';

const initialState = {
  chats: [],
  loading: false,
  error: null,
};

const chatListReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CHAT_LIST_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_CHAT_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        chats: action.chats,
      };
    case FETCH_CHAT_LIST_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        chats: [],
      };
    case REMOVE_CHAT_LIST_FROM_STORE:
      return {
        ...state,
        loading: false,
        error: null,
        chats: [],
      };
    default:
      return state;
  }
};

export default chatListReducer;
