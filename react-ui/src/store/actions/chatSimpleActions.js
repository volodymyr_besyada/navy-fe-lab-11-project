import axios from 'axios';
import {
  FETCH_CHAT_SIMPLE_BEGIN,
  FETCH_CHAT_SIMPLE_SUCCESS,
  FETCH_CHAT_SIMPLE_FAILURE,
  REMOVE_CHAT_SIMPLE_FROM_STORE,
  NEW_CHAT_MESSAGE,
} from './types';

const fetchChatSimpleBegin = () => ({
  type: FETCH_CHAT_SIMPLE_BEGIN,
});

const fetchChatSimpleSuccess = (messages) => ({
  type: FETCH_CHAT_SIMPLE_SUCCESS,
  messages,
});

const fetchChatSimpleFailure = (error) => ({
  type: FETCH_CHAT_SIMPLE_FAILURE,
  payload: { error },
});

export function fetchChatSimple(productId, owner, shopper) {
  return (dispatch) => {
    dispatch(fetchChatSimpleBegin());
    return axios.get(`/api/singleChat/${productId}/${owner}/${shopper}`)
      .then((messages) => {
        dispatch(fetchChatSimpleSuccess(messages.data));
      })
      .catch((error) => dispatch(fetchChatSimpleFailure(error)));
  };
}

export function removeChatSimpleFromStore() {
  return {
    type: REMOVE_CHAT_SIMPLE_FROM_STORE,
  };
}

const saveNewMessageLocal = (newMessage) => ({
  type: NEW_CHAT_MESSAGE,
  newMessage,
});
/* eslint-disable */
export function sendNewChatMessage(text, authorId, chatId, socket, recipient) {
  return (dispatch) => {
    return axios.post('/newMessage', {
      text, authorId, chatId,
    })
      .then((response) => {
        socket.emit('new message', { from: authorId, to: recipient, newMessage: response.data });
        dispatch(saveNewMessageLocal(response.data));
      })
      .catch((error) => console.log(error)); // eslint-disable-line no-console
  };
}
/* eslint-enable */
const newChatMessage = (newMessage) => ({
  type: NEW_CHAT_MESSAGE,
  newMessage,
});

export function getNewChatMessage(newMessage) {
  return (dispatch) => {
    dispatch(newChatMessage(newMessage));
  };
}
