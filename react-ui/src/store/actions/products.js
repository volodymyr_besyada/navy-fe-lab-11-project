import axios from 'axios';

export const FETCH_PRODUCTS_BEGIN = 'FETCH_PRODUCTS_BEGIN';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';
export const SORT_BY = 'SORT_BY';
export const FILTER_PRODUCTS = 'FILTER_PRODUCTS';


export const fetchProductsBegin = () => ({
  type: FETCH_PRODUCTS_BEGIN,
});

export const fetchProductsSuccess = (products) => ({
  type: FETCH_PRODUCTS_SUCCESS,
  payload: { products },
});

export const fetchProductsFailure = (error) => ({
  type: FETCH_PRODUCTS_FAILURE,
  payload: { error },
});

export const sortBy = (sortKey) => ({
  type: SORT_BY,
  sortKey,
});

export const filter = (products) => ({
  type: FILTER_PRODUCTS,
  products,
});

export function fetchProducts(id) {
  return (dispatch) => {
    dispatch(fetchProductsBegin(id));
    return axios.post('/api/products', { userId: id })
      // .then(handleErrors)
      .then((resp) => {
        dispatch(fetchProductsSuccess(resp.data));
        return resp.data;
      })
      .catch((error) => dispatch(fetchProductsFailure(error)));
  };
}

export const sortByKey = (sortKey = 'asc') => (dispatch) => {
  dispatch(sortBy(sortKey));
  return Promise.resolve();
};

export const filterProducts = (products = []) => (dispatch) => {
  dispatch(filter(products));
  return Promise.resolve();
};
