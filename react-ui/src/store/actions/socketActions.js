/* eslint-disable import/prefer-default-export */
import io from 'socket.io-client';

import { SOCKET_CONNECTION } from './types';

export function createSocketConnection(userId) {
  // const socket = io('http://localhost:5000', { query: `userId=${userId}` }); use this line on localhost
  const socket = io('/', { query: `userId=${userId}`, transports: ['websocket'] }); // use this on heroku
  return {
    type: SOCKET_CONNECTION,
    socket,
  };
}
