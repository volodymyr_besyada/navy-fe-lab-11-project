import { STORE_USER, USER_LOGOUT } from './types';

export const storeUser = (user) => async (dispatch) => {
  dispatch({
    type: STORE_USER,
    user,
  });
};

export const clearUser = () => async (dispatch) => {
  dispatch({
    type: USER_LOGOUT,
  });
};
