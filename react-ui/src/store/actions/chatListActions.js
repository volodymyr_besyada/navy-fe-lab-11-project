import axios from 'axios';
import {
  FETCH_CHAT_LIST_BEGIN,
  FETCH_CHAT_LIST_SUCCESS,
  FETCH_CHAT_LIST_FAILURE,
  REMOVE_CHAT_LIST_FROM_STORE,
} from './types';

const fetchChatListBegin = () => ({
  type: FETCH_CHAT_LIST_BEGIN,
});

const fetchChatListSuccess = (chats) => ({
  type: FETCH_CHAT_LIST_SUCCESS,
  chats,
});

const fetchChatListFailure = (error) => ({
  type: FETCH_CHAT_LIST_FAILURE,
  payload: { error },
});

export function fetchChatList(userID) {
  return (dispatch) => {
    dispatch(fetchChatListBegin());
    return axios.get(`/api/myChats/${userID}`)
      .then((chats) => {
        dispatch(fetchChatListSuccess(chats.data));
      })
      .catch((error) => dispatch(fetchChatListFailure(error)));
  };
}

export function removeChatListFromStore() {
  return {
    type: REMOVE_CHAT_LIST_FROM_STORE,
  };
}
