import axios from 'axios';

export const FETCH_PRODUCTDETAIL_BEGIN = 'FETCH_PRODUCTDETAIL_BEGIN';
export const FETCH_PRODUCTDETAIL_SUCCESS = 'FETCH_PRODUCTDETAIL_SUCCESS';
export const FETCH_PRODUCTDETAIL_FAILURE = 'FETCH_PRODUCTDETAIL_FAILURE';


export const fetchProductDetailBegin = () => ({
  type: FETCH_PRODUCTDETAIL_BEGIN,
});

export const fetchProductDetailSuccess = (product) => ({
  type: FETCH_PRODUCTDETAIL_SUCCESS,
  payload: { product },
});

export const fetchProductDetailFailure = (error) => ({
  type: FETCH_PRODUCTDETAIL_FAILURE,
  payload: { error },
});


export function fetchProductDetail(id) {
  return (dispatch) => {
    dispatch(fetchProductDetailBegin());
    if (id !== undefined) {
      return axios.get(`/api/product/${id}`)
        .then((resp) => {
          dispatch(fetchProductDetailSuccess(resp.data));
          return resp.data;
        });
    }
    return true;
  };
}
