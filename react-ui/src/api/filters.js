const filters = {
  categories: [
    {
      id: 1,
      value: 'book',
      title: 'Books',
    },
    {
      id: 2,
      value: 'tourist equipment',
      title: 'Tourist Equipment',
    },
    {
      id: 3,
      value: 'instruments',
      title: 'Instruments',
    },
    {
      id: 4,
      value: 'other',
      title: 'Other',
    },
  ],
};

export default filters;
