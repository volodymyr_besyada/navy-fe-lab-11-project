## Deploy to Heroku

```bash
git clone https://github.com/mars/heroku-cra-node.git
cd heroku-cra-node/
heroku create
git push heroku master
```

## Switching from create-react-app-buildpack

1. Remove **create-react-app-buildpack** from the app; [heroku/nodejs buildpack](https://devcenter.heroku.com/articles/nodejs-support#activation) will be automatically activated
  
    ```bash
    heroku buildpacks:clear
    ```
1. Move the root React app files (including dotfiles) into a `react-ui/` subdirectory

    ```bash
    mkdir react-ui
    git mv -k [!react-ui]* react-ui/
    mv node_modules react-ui/
    
    # If you see "fatal: Not a git repository", then fix that error
    mv react-ui/.git ./
    ```
1. Create a root [`package.json`](package.json), [`server/`](server/), & [`.gitignore`](.gitignore) modeled after the code in this repo
1. Commit and deploy
  
    ```bash
    git add -A
    git commit -m 'Migrate from create-react-app-buildpack to Node server'
    git push heroku master
    ```
1. If the app uses [Runtime configuration](https://github.com/mars/create-react-app-buildpack/blob/master/README.md#user-content-runtime-configuration), then follow [Runtime config](#user-content-runtime-config) below to keep it working.

## Runtime Config

1. Add the inner buildpack to your app, so that the `heroku/nodejs` buildpack is last:

   ```bash
   heroku buildpacks:add -i 1 https://github.com/mars/create-react-app-inner-buildpack
   
   heroku buildpacks
   ```
2. Set the bundle location for runtime config injection:

   ```bash
   heroku config:set JS_RUNTIME_TARGET_BUNDLE=/app/react-ui/build/static/js/*.js
   ```
3. Now, build the app with this new setup:

   ```bash
   git commit --allow-empty -m 'Enable runtime config with create-react-app-inner-buildpack'
   git push heroku master
   ```

## Local Development

1. **Node API server** at the root `./`
1. **React UI** in `react-ui/` directory.

### Run the API server

```bash
# Initial setup
npm install

# Start the server
npm start
```

#### Install new npm packages for Node

```bash
npm install package-name --save
```


### Run the React UI

In a separate terminal from the API server, start the UI:

```bash
# Always change directory, first
cd react-ui/

# Initial setup
npm install

# Start the server
npm start
```

#### Install new npm packages for React UI

```bash
# Always change directory, first
cd react-ui/

npm install package-name --save
```